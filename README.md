# Локальная разработка. Проект Magento 2 на Docker
Данный репозиторий позволит вам создать проект Magento 2 Community Edition для локальной разработки на основе LAMP с использованием docker-compose.

**Состав стека:**
- PHP 7.4.28
- Apache 2
- Mariadb 10.4.24
- Elasticsearch 7.13.1 (поддержка кириллицы)
- RabbitMQ (включает админку)
- Redis 5.0.13
- Xdebug 3

Вам понадобятся собственные:
- id_rsa
- id_rsa.pub
- public key
- private key
- github-token

Ориентировочное время развертывания: **20 минут.**

## Вариант #1
### Чистый проект на основе composer create-project

_Последовательность действий_

1. Создаём папку проекта в нужном месте и находясь в ней склонируйте данный репозиторий:
   - mkdir my_project
   - cd my_project
   - `git clone git@gitlab.com:agrocoder/m2docker.git docker`
2. Копируем ключи id_rsa и id_rsa.pub в директорию `docker/lamp/ident`
3. Вписываем свои креды в `docker/lamp/ident/auth.json`
4. Регистрируем домен в /etc/hosts
    - 127.0.0.1 lamp.empty.localhost
5. Отключаем xdebug на время установки
   - комментируем `[xdebug]` в файле `docker/lamp/config/xdebug.ini`
6. Запускаем обвязку
    - cd docker/lamp/
    - docker network create ept
    - docker-compose up -d --build
7. Входим в контейнер
    - docker exec -it ept-web bash
8. Меняем владельца и группу
    - sudo chown -R app:www-data /var/www/magento
    - sudo chmod 400 /home/app/.ssh/id_rsa
9. Создаем проект (соглашаемся на всё)
    - `composer create-project --repository=https://repo.magento.com/ magento/project-community-edition:2.4.3-p1 .`
10. Подготавливаем файлы
     - find var generated vendor pub/static pub/media app/etc -type f -exec chmod g+w {} +
     - find var generated vendor pub/static pub/media app/etc -type d -exec chmod g+ws {} +
     - chmod u+x bin/magento
     - sudo chown -R app:www-data .
11. Перезагружаем Apache
     - sudo a2ensite lamp.empty.localhost
     - sudo service apache2 reload
12. Проверяем xdebug-конфиг
    - cat /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
13. Выполняем установку
```shell
bin/magento setup:install \
--base-url=http://lamp.empty.localhost \
--db-host=ept-db \
--db-name=magento \
--db-user=root \
--db-password=root \
--cleanup-database \
--backend-frontname=admin \
--admin-firstname=Admin \
--admin-lastname=Cool \
--admin-email=admin.cool@gmail.com \
--admin-user=magento  \
--admin-password=1972magento  \
--language=en_US \
--currency=USD \
--timezone=America/Chicago \
--use-rewrites=1 \
--elasticsearch-host=ept-elastic
```
12. Настраиваем среду
    - bin/magento deploy:mode:set developer
    - bin/magento module:disable Magento_TwoFactorAuth
13. Настраиваем среду
```shell
rm -rf generated/*

rm -rf pub/static/* && \
rm -rf var/cache/* && \
rm -rf var/composer_home/* && \
rm -rf var/generation/* && \
rm -rf var/page_cache/* && \
rm -rf var/view_preprocessed/*

bin/magento c:f
bin/magento setup:upgrade --keep-generated
bin/magento setup:di:compile
bin/magento setup:static-content:deploy -f
bin/magento indexer:reindex
bin/magento c:f
```
14. Получаем IP для подключения к БД в IDE
    - exit (выходим из контейнера)
    - docker inspect ept-db
        - "IPAddress": "172.20.0.6"
15. Подключаемся к БД
    - host: 172.20.0.6
    - database: magento
    - login: root
    - password: root
16. Открываем проект в браузере
    - http://lamp.empty.localhost
17. Входим в админку `http://lamp.empty.localhost/admin`
    - логин: magento
    - пароль: 1972magento
18. Включаем xdebug
    - удаляем комментарии в файле `docker/lamp/config/xdebug.ini`
    - пересобираем контейнер ept-web

## Вариант #2
### Проект на основе ваших composer-файлов и модулей

Существует возможность использовать данный репозиторий, когда у вас есть проект содержащий модули собственной разработки и уникальное содержимое composer-файлов. В данном случае, обязательным условием являются наличие
- директории magento с composer-файлами
- модулей в директории magento/app/code
- готовых файлов
- app/etc/config.php
- app/etc/env.php
- дампа базы данных проекта-близнеца

Основное **отличие от первого варианта** - отсутствие необходимости выполнять bin/magento setup:install

Внимание! 
- папка `docker` должна быть на одном уровне с директорией `magento`, внутри которой будут находиться файлы из вашего репозитория!
- в директорию `/config/` нужно положить файлы (если их нет внутри `magento`)
  - env.php
  - config.php
- необходимо доработать файл `docker-compose.yml`
  - в разделе `magento-web:` дописать `volumes`
    - `- ./config/env.php:/var/www/magento/app/etc/env.php`
    - `- ./config/config.php:/var/www/magento/app/etc/config.php`

_Последовательность действий_

1. Создаём папку проекта в нужном месте и находясь в ней склонируйте данный репозиторий:
    - mkdir my_project
    - cd my_project
    - `git clone git@gitlab.com:agrocoder/m2docker.git docker`
2. Копируем ключи id_rsa и id_rsa.pub в директорию `docker/lamp/ident`
3. Вписываем свои креды в `docker/lamp/ident/auth.json`
4. Регистрируем домен в /etc/hosts
    - 127.0.0.1 lamp.empty.localhost
5. Отключаем xdebug на время установки
    - комментируем `[xdebug]` в файле `docker/lamp/config/xdebug.ini`
6. Запускаем обвязку
    - cd docker/lamp/
    - docker network create ept
    - docker-compose up -d --build
7. Входим в контейнер
    - docker exec -it ept-web bash
8. Меняем владельца и группу
    - sudo chown -R app:www-data /var/www/magento
    - sudo chmod 400 /home/app/.ssh/id_rsa
9. Выполняем команду (соглашаемся на всё)
    - composer install
10. Подготавливаем файлы
    - find var generated vendor pub/static pub/media app/etc -type f -exec chmod g+w {} +
    - find var generated vendor pub/static pub/media app/etc -type d -exec chmod g+ws {} +
    - chmod u+x bin/magento
    - sudo chown -R app:www-data .
11. Перезагружаем Apache
    - sudo a2ensite lamp.empty.localhost
    - sudo service apache2 reload
12. Проверяем xdebug-конфиг
    - cat /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
13. Импортируем свой дамп
    - exit (выходим из контейнера)
    - docker exec -i srv-db mysql -hlocalhost -uroot -proot magento < /path/to/dump/start.sql
14. Настраиваем среду
    - docker exec -it srv-web bash
    - bin/magento deploy:mode:set developer
    - bin/magento module:disable Magento_TwoFactorAuth
15. Настраиваем среду
```shell
rm -rf generated/*

rm -rf pub/static/* && \
rm -rf var/cache/* && \
rm -rf var/composer_home/* && \
rm -rf var/generation/* && \
rm -rf var/page_cache/* && \
rm -rf var/view_preprocessed/*

bin/magento c:f
bin/magento setup:upgrade --keep-generated
bin/magento setup:di:compile
bin/magento setup:static-content:deploy -f
bin/magento indexer:reindex
bin/magento c:f
```
16. Получаем IP для подключения к БД в IDE
    - exit (выходим из контейнера)
    - docker inspect srv-db
        - "IPAddress": "172.20.0.6"
17. Подключаемся к БД
    - host: 172.20.0.6
    - database: magento
    - login: root
    - password: root
18. Открываем проект в браузере
    - http://lamp.empty.localhost
19. Входим в админку `http://lamp.empty.localhost/admin`
    - логин: magento
    - пароль: 1972magento
20. Включаем xdebug
    - удаляем комментарии в файле `docker/lamp/config/xdebug.ini`
    - пересобираем контейнер ept-web